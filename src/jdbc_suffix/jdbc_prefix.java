/**
 * 
 */
package jdbc_suffix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * @author renop
 *
 */
public class jdbc_prefix 
{
	private static final Logger logger = LogManager.getLogger("DBI");

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		// Reading data using readLine 

		String pid = "";
		try 
		{
			System.out.print("Please enter PID: ");
			pid = reader.readLine(); 
			logger.debug("Input ok");
		}
		catch(IOException e) 
		{
			logger.error("Input failed");
			pid = "0";
		}
		//int pid = Integer.parseInt(name);
		jdbc_interface.getContentFromProducts(false,pid);
		logger.info("Hello Postfix");
	}
}
