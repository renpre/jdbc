package jdbc_suffix;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class jdbc_interface 
{
	private static String userName = "dbi";
	private static String password = "dbi_pass";
	private static String url = "jdbc:mysql://keller-net.feste-ip.net:64388/";
	private static String db = "dbi";
	private static String driver = "com.mysql.cj.jdbc.Driver";
	private static Connection con = null;
	private static final Logger logger = LogManager.getLogger("DBI");
	
	private static void connect() throws Exception 
	{
		con = null;
		Class.forName(driver);
		con = DriverManager.getConnection(url + db+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin", userName, password);
	}
	public static void getContentFromProducts(boolean selectAll, String pid) 
	{
		try 
		{
			connect();
			try 
			{
				Statement st = con.createStatement();
				System.out.println("----------------------");
				String sql = "SELECT * FROM products WHERE pid = '"+pid+"'";
				System.out.println("|"+sql+"|");
				ResultSet rs = st.executeQuery(sql);
				// iterate through the java resultset
				System.out.println("|------------------------------------");
				boolean is_first = true;
				while (rs.next())
				{
					if(is_first) 
					{
						is_first=false;
					}else 
					{
						System.out.println("|-------|-------|---------------|-|");
					}
					String PID = rs.getString("PID");
					String PNAME = rs.getString("PNAME");
					String CITY = rs.getString("CITY");
					String QUANTITY = rs.getString("QUANTITY");
					String PRICE = rs.getString("PRICE");
			        
			        // print the results
					System.out.println("|" + PID + "\t|" + PNAME + "\t|" + CITY + "\t|" + QUANTITY + "|"+PRICE+"|");
				}
				System.out.println("-------------------------------------");
				rs.close();
				st.close();
				
			} catch (SQLException s) 
			{
				logger.error("SQL statement is not executed! Error is: " + s.getMessage());
			}
			con.close();
		} catch (Exception e) 
		{
			logger.error("DB conncetion failed!");
			//e.printStackTrace();
		}
	}
	public static void getContentFromProducts(boolean selectAll) 
	{
		getContentFromProducts(true,"");
	}
}
